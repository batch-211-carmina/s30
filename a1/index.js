//count operator - to count the total number of fruits on sale
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $count: "fruitOnSale" }
	// { $group: { _id: null, fruitOnSale: { $sum: "$onSale" } } },
	// { $project: {_id: 0}}
]);


//count operator - to count the total number of fruits with stock more than 20
db.fruits.aggregate([
	{ $match: {stock: {$gte: 20}}},
	{ $count: "enoughStock" }
	// { $group: { _id: null, enoughStock: { $sum: "$stock" } } },
	// { $project: {_id: 0}}
]);


//average operator to get the average price of fruits onSale per supplier
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);


//max operator to get the highest price of a fruit per supplier
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
]);


//min operator to get the lowest price of a fruit per supplier
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
]);